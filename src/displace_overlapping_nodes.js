// This script can be run by the "scripting" plugin that comes with JOSM. It
// displaces selected nodes that overlap radially around their current location.

(function() {
    function getPointOnCircle(angle) {
        var radius = 2;
        return { x: radius * Math.cos(angle), y: radius * Math.sin(angle) };
    }

    function convertRadToDeg(rad) {
        return rad * 180 / Math.PI;
    }

    function groupNodes(nodes) {
        var groupedNodes = [];
        for (var i=0; i<nodes.length; i++) {
            node = nodes[i];
            var isInGroup = false;
            for (var n = 0; n < groupedNodes.length; n++) {
                if (groupedNodes[n].lon == node.lon && groupedNodes[n].lat == node.lat) {
                    groupedNodes[n].nodes.push(node);
                    isInGroup = true;
                }
            }
            if ( ! isInGroup) {
                groupedNodes.push({
                    lon: node.lon,
                    lat: node.lat,
                    nodes: [node]
                });
            }
        }
        return groupedNodes;
    }

    function displaceNodes(nodes) {
        var displacementAngle = Math.PI * 2 / nodes.length;
        var earthRadius = 6378137;

        for (var i=0; i<nodes.length; i++) {
            var displacementInMeters = getPointOnCircle(displacementAngle * i);
            var yDisplacementInRadians = displacementInMeters.y / earthRadius;
            var xDisplacementInRadians = displacementInMeters.x / (earthRadius * Math.cos(Math.PI * nodes[i].lat / 180));
            nodes[i].lat = nodes[i].lat + convertRadToDeg(yDisplacementInRadians);
            nodes[i].lon = nodes[i].lon + convertRadToDeg(xDisplacementInRadians);
        }
    }

    var groupedNodes = groupNodes(josm.layers.activeLayer.data.getSelectedNodes().toArray());
    for (var i=0; i<groupedNodes.length; i++) {
        displaceNodes(groupedNodes[i].nodes);
    }
})();
