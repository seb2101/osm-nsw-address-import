## Conflation Plugin Instructions

1. Install the following plugins in JOSM. (You will need to restart JOSM after installing them)
    - [Utilsplugin2](http://wiki.openstreetmap.org/wiki/JOSM/Plugins/utilsplugin2)
    - [Conflation](https://wiki.openstreetmap.org/wiki/JOSM/Plugins/Conflation)

 ![conflation-window](img/conflation-window.png)

2. Load the import data for a suburb, and select all the address points (Cmd-A or Ctrl-A).
3. Click 'Configure' in the conflation plugin, then click 'Freeze' for the **subject** layer. (The address points must still be selected from above for this to work.
![conflation-data-selection](img/conflation-data-selection.png)
4. Click 'Cancel' on the configuration window. (Don't worry. It will remember your selection.)
5. Download the OSM data for the desired region **IN A SEPARATE LAYER**.  
6. Make sure that the `Tags/Memberships` window is open.  
 (Use Menu: Windows→Tags/Memberships)
7. Select the place name of the suburb. It should appear in the `Tags/Memberships` window.
8. Right-click the boundary relation which has now appeared in the `Tags/Memberships` window, and choose `Select relation`.
 ![conflation-suburb-relation-selection](img/conflation-suburb-relation-selection.png)
9. From the JOSM Selection menu, choose `All inside` to select only things inside the relation.
10. Refine the selection to include only the buildings using Edit→Search (Cmd-F, or Ctrl-F).
  - Use the search string `building=* type:way OR preset:"Annotation/Address"`.
  - Use the selection setting `find in selection`
 
 ![conflation-building-selection](img/conflation-building-selection.png)
 
11. Open the conflation configuration window again, (making sure that the buildings remain selected)
12. Click 'Freeze' for the **reference** layer.
 ![conflation-configure-subject](img/conflation-configure-subject.png)
13. Leaving all other settings as default, click 'Generate Matches'.
14. Load the LPI NSW Base Map and the LPI NSW Imagery to check the matches.
16. Work through the generated matches.
![conflation-ready](img/conflation-ready.png)

## Hints
- You can jump to a potential conflation by double-clicking it in the conflation window.
- If a suggested conflation is invalid, select it in the conflation window, and click 'Remove'.
- The keyboard shortcut for conflation is `Ctrl-Option-F` or `Ctrl-Alt-F`, depending on your system.
- It can sometimes be quicker to work your way down a street, rather than down the conflations list. Selecting an address point will automatically select its conflation.
 ![conflation-shortcuts](img/conflation-shortcuts.mp4)
- If the data is all in one layer, because it has already been uploaded to OSM, we can conflate it by just doing the select and freeze steps using careful search strings, rather than layers. Use search string `preset:"Annotation/Address" type:node` to select and freeze the address nodes as subject. Use search string `building=* type:way` to select and freeze the buildings as subject.
- I usually like to have the NSW LPI data as the current layer, and the OSM data as a background layer. This has the nice effect of making the buildings appear to 'pop' into the import layer when conflated.

## Validating the conflation
- Save the layer `SUBURB.osm`.
- Duplicate the layer `SUBURB.osm`.
- Merge the **duplicate** layer into the OSM data layer.
- Run the JOSM validation (Shift-V) to quickly check for duplicate house numbers, etc.
- If you are trying to validate data for a particular suburb, select the suburb boundary relation, then use 'Selection→All inside' from [Utilsplugin2](http://wiki.openstreetmap.org/wiki/JOSM/Plugins/utilsplugin2). You can then use Shift-V to validate *just* the selected suburb.